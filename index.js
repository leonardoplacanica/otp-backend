const emailService = require('./service/email');
const checkOtpService = require('./service/checkOtp');
const util = require('./utils/util');

const healthPath = '/health';
const emailPath = '/email';
const checkOtpPath = '/check-otp';

exports.handler = async (event) => {
    console.log('Request Event: ', event);
    let response;
    switch(true) {
        case event.httpMethod === 'GET' && event.path === healthPath:
            response = util.buildResponse(200);
            break;
        case event.httpMethod === 'POST' && event.path === emailPath:
            const emailBody = JSON.parse(event.body);
            response = await emailService.email(emailBody);
            break;
        case event.httpMethod === 'POST' && event.path === checkOtpPath:
            const checkOtpBody = JSON.parse(event.body);
            response = await checkOtpService.checkOtp(checkOtpBody);
            break;
        default:
            response = util.buildResponse(404, '404 Not Found');
    }
    return response;
};
