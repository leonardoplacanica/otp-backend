const AWS = require('aws-sdk');
AWS.config.update({
  region: 'us-east-2'
})
const util = require('../utils/util');

const dynamodb = new AWS.DynamoDB.DocumentClient();
const SES = new AWS.SES();

const userTable = 'users';
const from = "leonardo.placanica@gmail.com"

function randomString(length, chars) {
  var result = '';
  for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
}

async function email(userInfo) {
  const email = userInfo.email;
  const randomCode = randomString(6, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
  
  if (!email) {
    return util.buildResponse(401, {
      message: 'All fields are required'
    })
  }

  const dynamoUser = await getUser(email.toLowerCase().trim());
  if (dynamoUser && dynamoUser.email) {
    const updateUserResponse = await updateUser({email: dynamoUser.email, otp: randomCode});

    if (!updateUserResponse) {
      return util.buildResponse(503, { message: 'Server Error. Please try again later.'});
    }
  
  }
  else{
    const user = {
      email: email,
      otp: randomCode
    }
  
    const saveUserResponse = await saveUser(user);
    if (!saveUserResponse) {
      return util.buildResponse(503, { message: 'Server Error. Please try again later.'});
    }

  }

  const params = {
    Destination: {
        ToAddresses: [email],
    },
    Message: {
        Body: {
            Text: { Data: "Your code is " + randomCode },
        },
        Subject: { Data: "Otp code" },
    },
    Source: from,
  };

  try {
    await SES.sendEmail(params).promise();
  } catch (error) {
      console.log('error sending email ', error);
      return util.buildResponse(400, { error: "Email failed on sending" });
  }

  return util.buildResponse(200, { email: email });

}

async function getUser(email) {
  const params = {
    TableName: userTable,
    Key: {
      email
    }
  }

  return await dynamodb.get(params).promise().then(response => {
    return response.Item;
  }, error => {
    console.error('There is an error getting user: ', error);
  })
}

async function saveUser(user) {
  const params = {
    TableName: userTable,
    Item: user
  }
  return await dynamodb.put(params).promise().then(() => {
    return true;
  }, error => {
    console.error('There is an error saving user: ', error)
  });
}

async function updateUser(newUser){
  var params = {
    TableName: userTable,
    Key: {email: newUser.email},
    UpdateExpression: "set otp = :val",
    ExpressionAttributeValues:{
      ":val": newUser.otp
    },
    ReturnValues:"UPDATED_NEW"
  };
  return await dynamodb.update(params).promise().then(() => {
    return true;
  }, error => {
    console.error('There is an error updating user: ', error)
  });
}

module.exports.email = email;