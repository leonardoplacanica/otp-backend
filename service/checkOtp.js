const AWS = require('aws-sdk');
AWS.config.update({
  region: 'us-east-2'
})
const util = require('../utils/util');

const dynamodb = new AWS.DynamoDB.DocumentClient();
const userTable = 'users';

async function checkOtp(user) {
  const username = user.email;
  const password = user.otp;
  if (!user || !username || !password) {
    return util.buildResponse(401, {
      message: 'username and password are required'
    })
  }

  const dynamoUser = await getUser(username.toLowerCase().trim());
  if (!dynamoUser || !dynamoUser.email) {
    return util.buildResponse(403, { message: 'user does not exist'});
  }

  if (password !== dynamoUser.otp) {
    return util.buildResponse(403, { message: 'otp is incorrect'});
  }

  const userInfo = {
    email: dynamoUser.email,
    otp: dynamoUser.otp
  }
  
  const response = {
    user: userInfo
  }
  return util.buildResponse(200, response);
}

async function getUser(email) {
  const params = {
    TableName: userTable,
    Key: {
      email
    }
  }

  return await dynamodb.get(params).promise().then(response => {
    return response.Item;
  }, error => {
    console.error('There is an error getting user: ', error);
  })
}

module.exports.checkOtp = checkOtp;